import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { restaurant } from '../models/menu'
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }
  fetchApi(url: string): Observable<restaurant>{
   return this.http.get<restaurant>(url);
  }
}

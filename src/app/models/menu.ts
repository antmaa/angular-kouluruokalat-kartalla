export interface restaurant {
    RestaurantName: string | null;
    RestaurantUrl: string| null;
    PriceHeader: string | null;
    Footer: string | null;
    MenusForDays: menu[] | null;
}

export interface menu {
    Date: string | null;
    LunchTime: string | null;
    SetMenus: setmenus[] | null
}
interface setmenus {
    SortOrder: number | null;
    Name: string | null;
    Price: string | null;
    Components: components[] | null;
}
interface components {
    name: string | null;
}
import { Component, ViewEncapsulation } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
  /* encapsulation: ViewEncapsulation.None */
})
export class AppComponent {
  //siivoa jos ei toimi
  bgImg: string = "/assets/img/kartta.png";
  // for making the right components visible with ngIf
  open: boolean;
  fiilu: boolean;
  aimo: boolean;
  piato: boolean;
  rentukka: boolean;
  lozzi: boolean;
  ylisto: boolean;

  // when the element is pressed the class is changed from relative position to static
  static: boolean = false;

  constructor() {}

  //gets emitted restaurant name from map component which got it emitted by restaurant component
  onOpened(name: string) {
    console.log(name);
    switch (name.replace(/\s/g, "")) {
      case "Fiilu": {
        this.fiilu = true;
        break;
      }
      case "RavintolaPiato": {
        this.piato = true;
        break;
      }
      case "Aimo": {
        this.aimo = true;
        break;
      }
      case "RavintolaRentukka": {
        this.rentukka = true;
        break;
      }
      case "RavintolaLozzi": {
        this.lozzi = true;
        break;
      }
      case "RavintolaYlistö": {
        this.ylisto = true;
        break;
      }
    }
  }
  onClosed() {
    this.fiilu = false;
    this.aimo = false;
    this.piato = false;
    this.rentukka = false;
    this.lozzi = false;
    this.ylisto = false;
  }
}

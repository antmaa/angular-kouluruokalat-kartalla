import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ApiService } from '../service/api.service';
import { restaurant, menu } from '../models/menu';
import { formatDate } from "@angular/common";
@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.scss']
})
export class RestaurantComponent implements OnInit {
  @Input() url: string;
  @Input() img: string;
  @Input() name: string;
  data$: restaurant;
  // from observable to object
  menu: menu[];
  currentMenu: menu;
  restaurantClosed: boolean;
  // for opening and closing the component, other components are closed and opened
  @Output() opened = new EventEmitter<string>();
  @Output() closed = new EventEmitter<string>();
  isOpen: boolean = false;
  // -- get current date ------------------------------------------
  today: number = Date.now();
  format: string = 'yyyy-MM-dd';
  format2: string = 'HHmm';
  locale = 'en-US';
  formattedDate = formatDate(this.today, this.format, this.locale);
  formattedFormattedDate = this.formattedDate + "T00:00:00+01:00";
  time = formatDate(this.today, this.format2, this.locale);
  // -- for mouse over event--------------------------------------------
  mouseOvered: boolean = false;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.fetchApi(this.url).subscribe(data => this.data$ = data);
    console.log(this.data$);
    console.log(this.formattedDate);
    console.log(this.formattedFormattedDate);
    console.log(this.time);

  }
  
  getMenu(){
    this.menu = this.data$.MenusForDays;
    for(let i = 0; i < this.menu.length; i++){
      console.log(this.menu[i].Date);
      
      if (this.menu[i].Date === this.formattedFormattedDate ) {
        // If the lunch time strings last four numbers are smaller than current time in format HHmm the restaurant is closed
        if (parseInt(this.menu[i].LunchTime.replace(/\D/g,'').slice(-4), 10) < parseInt(this.time)
        || 
        //same for the first four
        parseInt(this.menu[i].LunchTime.replace(/\D/g,'').slice(0, -4), 10) > parseInt(this.time)) {
          this.restaurantClosed = true;
          this.menu[i].LunchTime = this.menu[i].LunchTime + "(suljettu)"
        } 
        console.log(parseInt(this.menu[i].LunchTime.replace(/\D/g,'').slice(0, -4), 10));
        return this.menu[i];
    }
    // if 
      else{
        this.restaurantClosed = true;
        return  {
          "Date": "",
          "LunchTime": "SULJETTU TÄNÄÄN",
          "SetMenus": []
        }
      }
  }
}
  open(){
    this.currentMenu = this.getMenu();
    this.isOpen = true;
    this.opened.emit(this.data$.RestaurantName.replace(/\s/g, ""));
    console.log(this.currentMenu);
  }
  del(){
    this.currentMenu = null;
    this.restaurantClosed = false;
    this.isOpen = false;
    this.closed.emit();
  }
}

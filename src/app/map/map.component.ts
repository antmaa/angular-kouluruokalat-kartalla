import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent {
  url1: string = '/assets/data/mock.json';
  url2: string = '/assets/data/mock2.json';
  url3: string = '/assets/data/mock3.json';
  url4: string = '/assets/data/mock4.json';
  url5: string = '/assets/data/mock5.json';
  url6: string = '/assets/data/mock6.json';
  
  img1: string = '/assets/img/piato.jpg';
  img2: string = '/assets/img/fiilu.png';
  img3: string = '/assets/img/aimo.jpg';
  img4: string = '/assets/img/rentukka.PNG';
  img5: string = '/assets/img/lozzi.PNG';
  img6: string = '/assets/img/ylisto.PNG';
  
  bgImg: string = "/assets/img/kartta.png";
  // for making the right components visible with ngIf
  open: boolean;
  fiilu: boolean;
  aimo: boolean;
  piato: boolean;
  rentukka: boolean;
  ylisto: boolean;
  lozzi: boolean;
  //names, so they are accessible before initializing the menu
  fiiluN: string = "Fiilu";
  aimoN: string  = "Aimo";
  piatoN: string = "Piato";
  rentukkaN: string = "Rentu";
  lozziN: string  = "Lozzi";
  ylistoN: string = "Ylistö";
  // when the element is pressed the class is changed from relative position to static
  static: boolean = false;
  
  @Output() opened = new EventEmitter<string>();
  @Output() closed = new EventEmitter<string>();
  constructor() { }

  onOpened(name:string){
    this.static = true;
    //emit the same message one level higher
    this.opened.emit(name.replace(/\s/g, ""));
    //----
    switch(name.replace(/\s/g, "")) { 
      case "Fiilu": { 
         this.open = true;
         this.fiilu = true;
         break; 
      } 
      case "RavintolaPiato": { 
        this.open = true;
        this.piato = true; 
         break; 
      } 
      case "Aimo": { 
        this.open = true;
        this.aimo = true; 
         break; 
      } 
      case "RavintolaRentukka": { 
        this.open = true;
        this.rentukka = true; 
         break; 
      } 
      case "RavintolaLozzi": { 
        this.open = true;
        this.lozzi = true; 
         break; 
      } 
      case "RavintolaYlistö": { 
        this.open = true;
        this.ylisto = true; 
         break; 
      } 
   } 
  }
  onClosed(){
    this.static = false;
    this.open = false;
    this.aimo = false;
    this.fiilu = false;
    this.piato = false;
    this.rentukka = false;
    this.lozzi = false;
    this.ylisto = false;
    this.closed.emit();
  }
}
